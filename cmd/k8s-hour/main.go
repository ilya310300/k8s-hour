package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"
)

const (
	host = ""
	port = 80
)

func main() {
	go initHttpServer()
	fmt.Println("App started")
	var i int64
	for {
		i++
		fmt.Printf("Uptime is %d sec\n", i)
		time.Sleep(time.Second)
	}
}

func initHttpServer() {
	podIP := os.Getenv("MY_POD_IP")

	http.HandleFunc("/ready", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		_, _ = fmt.Fprintf(w, "{\"ready\": true}")
	})
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		_, _ = fmt.Fprintf(w, "<h1><center>Bonjour, je m'appelle K8S Hour Service.</center></h1>"+
			"<br>Now "+time.Now().String()+""+
			"<br>Pod IP: "+podIP+"")
	})
	appHost := fmt.Sprintf("%s:%d", host, port)
	err := http.ListenAndServe(appHost, nil)
	if err != nil {
		log.Fatal(err)
	}
}
