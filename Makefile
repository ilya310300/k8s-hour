PWD = $(shell pwd)
SERVICE_NAME=k8s-hour

# Запуск сервиса
.PHONY: build
build:
	go build -o bin/$(SERVICE_NAME) $(PWD)/cmd/$(SERVICE_NAME)

# Запуск сервиса
.PHONY: run
run:
	go run $(PWD)/cmd/$(SERVICE_NAME)
